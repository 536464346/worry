/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var React = require('react-native');
import TabBarNavigator from 'react-native-tabbar-navigator';
var AskList=require('./src/components/AskList');
var Login=require('./src/components/Login');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
} = React;
var AppInit=require('./src/AppInit');
AppInit();
var worry = React.createClass({
  render: function() {
    return (
      <TabBarNavigator
        navTintColor='ffffff'
        navBarTintColor='987eb9'
        tabTintColor='987eb9'
        tabBarTintColor='f5f5f5'
        onChange={(index)=>console.log(`selected index ${index}`)}>
        <TabBarNavigator.Item title='心事' defaultTab>
          
              <AskList/>
       
        </TabBarNavigator.Item>
        <TabBarNavigator.Item title='发现'>
          <View style={styles.tabContentStyle}>
              <Text>I am tab 1</Text>
          </View>
        </TabBarNavigator.Item>

        <TabBarNavigator.Item title='我的'>
              <Login/>
          
        </TabBarNavigator.Item>
      </TabBarNavigator>
    );
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});

AppRegistry.registerComponent('worry', () => worry);
