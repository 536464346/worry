var UserStore=require('./stores/UserStore');
var AskStore=require('./stores/AskStore');
var AppInit=function(){

	UserStore.userInit();
	AskStore.askInit();
}

module.exports=AppInit;