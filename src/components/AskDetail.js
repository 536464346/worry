'use strict';

var React = require('react-native');
var {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  ActionSheetIOS,
  TouchableOpacity,
  Component,
  AlertIOS
} = React;
var CommentList=require('./CommentList');
var WW=Dimensions.get('window').width;
var WH=Dimensions.get('window').height;
var BUTTONS = [
  '举报',
  '分享',
  '取消',
];
var FILTER_BUTTONS = [
  '未回复',
  '已回复',
  '全部',
];
var CANCEL_INDEX = 2;


var AskDetail = React.createClass({


  render: function() {
    return (
        <ScrollView>
          <View style={styles.row}>
            <Image style={styles.authorHeader} source={{uri:'http://q.qlogo.cn/qqapp/101268366/4E09D3A00FBB23172E9F3C7A0538F77D/100'}} />
            <View style={styles.authorInfo}>
              <View style={styles.userInfo}>
                <Text style={styles.userName}>
                  listentomeha
                </Text>
                <View style={styles.baseInfo}>
                  <Image style={styles.gender} source={require('../images/gender.png')}/>
                  <Text style={styles.age}>20</Text>
                </View>
              </View>
              <View>
                <Text style={styles.pubTime}>
                  8小时前
                </Text>
              </View>
            </View>
          </View>
          <TouchableHighlight>
            <View style={styles.row}>
              <Text style={styles.askTitle}>想结束和女下属的不正当关系</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight>
            <View style={styles.row}>
              <Text style={styles.text}>
               我已婚，有小孩。 老婆出轨，分居3年，没离婚。 她是我下属，丧偶。 因为学历悬殊，两人没啥共同话题。 但隔三差五会去开房，床上很和谐。 她在生活中很照顾我。 但是经常聊不到一块。 很苦恼，要不要结束这段关系？
              </Text>
            </View>
          </TouchableHighlight>
          <View style={styles.row}>

            <Text style={styles.askAttach}>6634 阅读 11 评论</Text>
          </View>

          <View style={[styles.row,styles.footer]}>
            <View style={styles.userInfo}>
            <Image source={require('../images/heart_empty.png')} style={styles.zanImg}/>
            <Text style={styles.zan}>11</Text>
            </View>
            <TouchableHighlight onPress={()=>this._pressMore()}>
              <Text style={styles.more}>...</Text>
            </TouchableHighlight>
          </View>

          <View style={styles.separator}/>

          <CommentList/>


          <View style={styles.toolBar}>
            <View style={styles.tools}>
              <Image source={require('../images/heart_empty.png')}/>
            </View>
            <View style={styles.tools}>
              <Image source={require('../images/heart_empty.png')}/>
            </View>
            <View style={styles.tools}>
              <Image source={require('../images/heart_empty.png')}/>
            </View>
          </View>
        </ScrollView>

    );
  },
  _pressMore:function(){
    console.log(1234);
     ActionSheetIOS.showActionSheetWithOptions({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  },
  _pressFilter:function(){
    ActionSheetIOS.showActionSheetWithOptions({
      options: FILTER_BUTTONS,
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  }
});

var THUMB_URLS = [
  'Thumbnails/like.png',
  'Thumbnails/dislike.png',
  'Thumbnails/call.png',
  'Thumbnails/fist.png',
  'Thumbnails/bandaged.png',
  'Thumbnails/flowers.png',
  'Thumbnails/heart.png',
  'Thumbnails/liking.png',
  'Thumbnails/party.png',
  'Thumbnails/poke.png',
  'Thumbnails/superlike.png',
  'Thumbnails/victory.png',
  ];
var LOREM_IPSUM = 'Lorem ipsum dolor sit amet, ius ad pertinax oportere accommodare, an vix civibus corrumpit referrentur. Te nam case ludus inciderint, te mea facilisi adipiscing. Sea id integre luptatum. In tota sale consequuntur nec. Erat ocurreret mei ei. Eu paulo sapientem vulputate est, vel an accusam intellegam interesset. Nam eu stet pericula reprimique, ea vim illud modus, putant invidunt reprehendunt ne qui.';

/* eslint no-bitwise: 0 */
var hashCode = function(str) {
  var hash = 15;
  for (var ii = str.length - 1; ii >= 0; ii--) {
    hash = ((hash << 5) - hash) + str.charCodeAt(ii);
  }
  return hash;
};

var styles = StyleSheet.create({
  container:{
    height:WH
  },
  authorHeader:{
    height:30,
    width:30,
    borderRadius:15,
    margin:5
  },
  authorInfo:{
    flex:1,
    flexDirection:'column',
    padding:5
  },
  userInfo:{
    flexDirection:'row'

  },
  userName:{
    color:'#987eb9'

  },
  pubTime:{
    color:'#aaaaaa',
    fontSize:10
  },
  baseInfo:{
    flexDirection:'row',
    backgroundColor:'#b6defa',
    height:16,
    marginTop:2,
    marginLeft:5
  },
  gender:{
    width:15,
    height:15

  },
  age:{
    color:'#ffffff',
    fontSize:12
  },
  askTitle:{
    fontSize:16,
    fontWeight:'bold',
    color:'#505050'
  },
  askAttach:{
    fontSize:10,
    color:'#aaaaaa'
  },
  footer:{
    justifyContent:'space-between'
  },
  zan:{
    paddingLeft:10,
    color:'#aaaaaa',

  },
  zanImg:{
    width:16,
    height:16
  },
  more:{
    color:'#aaaaaa',
    fontSize:16,
    fontWeight:'bold',
    paddingLeft:10,
    paddingRight:10
  },
  row: {
    flexDirection: 'row',
    padding: 10,
    paddingBottom:5,
    backgroundColor: '#F6F6F6',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
  thumb: {
    width: 64,
    height: 64,
  },
  text: {
    flex: 1,
    lineHeight:20,
    color:'#565656',
    fontSize:16
  },
  leftNavItem:{
    margin:10
  },
  toolBar:{
    position:'absolute',
    left:0,
    top:WH-40-60,
    flexDirection:'row',
    width:WW,
    borderTopColor:'#cccccc',
    borderTopWidth:1,
    height:40

  },
  tools:{
    flex:1,
    flexDirection:'row',
    justifyContent:'center',
  }

});

module.exports = AskDetail;