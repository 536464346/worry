'use strict';

var React = require('react-native');
var {
  TextInput,
  View,
  Image,
  StyleSheet,
  Dimensions,
  TouchableHighlight,
  AlertIOS,
  Text,
  Modal
} = React;

var Spinner=require('react-native-spinkit');
var UserActions=require("../actions/UserActions");
var UserStore=require("../stores/UserStore");
var WW=Dimensions.get('window').width;
var WH=Dimensions.get('window').height;
var AskDetail=require('./AskDetail');
var Login=React.createClass({
	getInitialState:function(){

		return UserStore.initState();

	},

	componentWillMount:function(){
		UserStore.update(this._updateState);
	},

	componentDidMount: function() {
    UserStore.addChangeListener(this._onChange);


  },

  componentWillUnmount: function() {
    UserStore.removeChangeListener(this._onChange);
  },

	render:function(){

		return (
			<View style={styles.container}>
				<Modal visible={false} transparent={true}>
					<View  style={styles.modal}>
						<View style={styles.loading}>
							<Text style={styles.loadingText}>登录中...</Text>
							<Spinner  isVisible={true} size={37} type='Circle' color='#ffffff'/>
						</View>
					</View>
				</Modal>
				<View style={styles.formControlContainer}>
					<Image style={styles.logo} source={require('../images/logo.png')}/>
				</View>
				<View style={[styles.formControlContainer,styles.topFormControlContainer]}>
					<TextInput
					style={styles.formControl}
				    editable={true}
				    onChangeText={(text) => this._onNameChange(text)}
				    placeHolder='用户名'
				    placeholderTextColor='#000000'
				    value={this.state.user_name}
				  />
			  </View>
			  <View style={styles.formControlContainer}>
				  <TextInput
				  	style={styles.formControl}
				    editable={true}
				    onChangeText={(text) => this._onPasswdChange(text)}
				    placeHolder='密码'
				    placeholderTextColor='#000000'
				    secureTextEntry={true}
				    value={this.state.passwd}
				  />
			  </View>

			  <View style={styles.formControlContainer}>
				  <TouchableHighlight onPress={()=>this._login()}>
				  	<Text style={styles.formText}>登录</Text>
				  </TouchableHighlight>
			  </View>

			  <View style={styles.loginAttach}>
			  	<Text style={[styles.attachText,styles.attachTextForget]}>无法登录?</Text><Text style={styles.attachText}>新用户</Text>
			  </View>
		  </View>

			);
	},
	_onNameChange:function(user_name){

		UserActions.userNameChange(user_name);
	},
	_onPasswdChange:function(passwd){
		UserActions.passWordChange(passwd);
	},
	_login:function(){
		if(!this.state.user_name || !this.state.passwd){
			AlertIOS.alert(
			  '用户名或密码为空'
			)
			return false;
		}
		UserActions.login();
	},
	_updateState:function(data){
		this.setState(data);
		
		
	},
	_onChange:function(){
		var that=this;
		UserStore.update(function(data){
			that._updateState(data);
			if(data.is_login){
				that.props.navigator.push({
		      title: '心事1234124',
		      component: <AskDetail/>,
		      passProps: {
		      }
		    });
			}
			
		});
	}


});

var styles=StyleSheet.create({

	container:{
		flexDirection:'column',
		alignItems:'center',
		backgroundColor:'#f5f5f5',
		height:WH
	},
	logo:{
		width:80,
		height:80,
		borderRadius:40,
		marginTop:30,
		marginBottom:10
	},
	formControlContainer:{
		width:WW,
		flexDirection:'row',
		justifyContent:'center'

	},
	topFormControlContainer:{
		borderBottomWidth:1,
		borderBottomColor:'#cccccc'
	},
	formControl:{
		height:40,
		width:WW,
		backgroundColor:'#ffffff',
		textAlign:'center'
	},
	formText:{
		height:40,
		width:WW-20,
		textAlign:'center',
		fontSize:16,
		fontWeight:'bold',
		color:'#ffffff',
		backgroundColor:'#987eb9',
		lineHeight:28,
		marginTop:10
	},
	loginAttach:{
		marginTop:15,
		flexDirection:'row',
		width:WW
	},
	attachText:{
		flex:1,
		padding:10,
		textAlign:'right',
		color:'#05A5D1'
	},
	attachTextForget:{
		textAlign:'left'
	},
	modal:{
		flex:1,
		flexDirection:'row',
		alignItems:'center',
		justifyContent:'center'
	},
	loadingText:{

		color:'#ffffff'
	},
	loading:{
		width:80,
		height:80,
		backgroundColor:'#000000',
		borderRadius:5,
		alignItems:'center',
		justifyContent:'center'
	}

});




module.exports=Login;