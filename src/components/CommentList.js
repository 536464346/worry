'use strict';

var React = require('react-native');
var {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  ActionSheetIOS,
  TouchableOpacity,
  Component,
  AlertIOS
} = React;
var AskDetail=require('./AskDetail');

var WW=Dimensions.get('window').width;
var WH=Dimensions.get('window').height;
var BUTTONS = [
  '举报',
  '分享',
  '取消',
];
var FILTER_BUTTONS = [
  '未回复',
  '已回复',
  '全部',
];
var CANCEL_INDEX = 2;


var CommentList = React.createClass({

  getInitialState: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      dataSource: ds.cloneWithRows(this._genRows({})),
    };
  },

  _pressData: ({}: {[key: number]: boolean}),

  componentWillMount: function() {
    this._pressData = {};
  },

  render: function() {
    return (
        <ListView
          style={styles.container}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          renderScrollComponent={()=>this._renderContainer()}

        />

    );
  },

  _renderRow: function(rowData: string, sectionID: number, rowID: number) {
    var rowHash = Math.abs(hashCode(rowData));
    var imgSource = {
      uri: THUMB_URLS[rowHash % THUMB_URLS.length],
    };
    return (
      
        <View>
          <View style={styles.row}>
            <Image style={styles.authorHeader} source={{uri:'http://q.qlogo.cn/qqapp/101268366/4E09D3A00FBB23172E9F3C7A0538F77D/100'}} />
            <View style={styles.authorInfo}>
              <View style={styles.userInfo}>
                <Text style={styles.userName}>
                  listentomeha
                </Text>
    
              </View>
            </View>
          </View>

          <TouchableHighlight onPress={() => this._pressRow(rowID)}>
            <View style={styles.row}>
              <View style={styles.zan}>
                <Text style={styles.zanNum}>0</Text>
              </View>
              <Text style={styles.text}>
               我已婚，有小孩。 老婆出轨，分居3年，没离婚。 她是我下属，丧偶。 因为学历悬殊，两人没啥共同话题。 但隔三差五会去开房，床上很和谐。 她在生活中很照顾我。 但是经常聊不到一块。 很苦恼，要不要结束这段关系？
              </Text>
            </View>
          </TouchableHighlight>
         
         

          <View style={styles.separator} />
        </View>
      
    );
  },

  _genRows: function(pressData: {[key: number]: boolean}): Array<string> {
    var dataBlob = [];
    for (var ii = 0; ii < 100; ii++) {
      var pressedText = pressData[ii] ? ' (pressed)' : '';
      dataBlob.push('Row ' + ii + pressedText);
    }
    return dataBlob;
  },

  _pressRow: function(rowID: number) {
    this.props.navigator.push({
      title: '心事1234124',
      component: <AskDetail/>,
      passProps: {
      }
    });
  },
  _pressMore:function(){
    console.log(1234);
     ActionSheetIOS.showActionSheetWithOptions({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  },
  _pressFilter:function(){
    ActionSheetIOS.showActionSheetWithOptions({
      options: FILTER_BUTTONS,
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  },
  _renderContainer:function(){
    return (
      <View></View>
      );
  }
});

var THUMB_URLS = [
  'Thumbnails/like.png',
  'Thumbnails/dislike.png',
  'Thumbnails/call.png',
  'Thumbnails/fist.png',
  'Thumbnails/bandaged.png',
  'Thumbnails/flowers.png',
  'Thumbnails/heart.png',
  'Thumbnails/liking.png',
  'Thumbnails/party.png',
  'Thumbnails/poke.png',
  'Thumbnails/superlike.png',
  'Thumbnails/victory.png',
  ];
var LOREM_IPSUM = 'Lorem ipsum dolor sit amet, ius ad pertinax oportere accommodare, an vix civibus corrumpit referrentur. Te nam case ludus inciderint, te mea facilisi adipiscing. Sea id integre luptatum. In tota sale consequuntur nec. Erat ocurreret mei ei. Eu paulo sapientem vulputate est, vel an accusam intellegam interesset. Nam eu stet pericula reprimique, ea vim illud modus, putant invidunt reprehendunt ne qui.';

/* eslint no-bitwise: 0 */
var hashCode = function(str) {
  var hash = 15;
  for (var ii = str.length - 1; ii >= 0; ii--) {
    hash = ((hash << 5) - hash) + str.charCodeAt(ii);
  }
  return hash;
};

var styles = StyleSheet.create({
  container:{
    height:WH
  },
  authorHeader:{
    height:30,
    width:30,
    borderRadius:15,
    margin:5
  },
  authorInfo:{
    flex:1,
    flexDirection:'column',
    padding:5
  },
  userInfo:{
    flexDirection:'row'

  },
  userName:{
    color:'#987eb9',
    marginTop:5

  },
  pubTime:{
    color:'#aaaaaa',
    fontSize:10
  },
  baseInfo:{
    flexDirection:'row',
    backgroundColor:'#b6defa',
    height:16,
    marginTop:2,
    marginLeft:5
  },

  zan:{
    width:30,
    height:20,
    borderRadius:2,
    backgroundColor:'#d0d0d0',
    margin:5,
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'

  },
  zanNum:{
    fontSize:12,
    color:'#565656',
    
  },
  more:{
    color:'#aaaaaa',
    fontSize:16,
    fontWeight:'bold',
    paddingLeft:10,
    paddingRight:10,
    

  },
  row: {
    flexDirection: 'row',
    padding: 10,
    paddingBottom:5,
    backgroundColor: '#F6F6F6',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
  thumb: {
    width: 64,
    height: 64,
  },
  text: {
    flex: 1,
    lineHeight:20,
    color:'#565656',
    fontSize:16
  },
  leftNavItem:{
    margin:10
  }
});

module.exports = CommentList;