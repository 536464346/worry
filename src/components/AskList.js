'use strict';

var React = require('react-native');
var {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Dimensions,
  ActionSheetIOS,
  TouchableOpacity,
  Component,
  AlertIOS
} = React;
var AskDetail=require('./AskDetail');
var AskStore=require('../stores/AskStore');
var AskAction=require('../actions/AskActions');
var RefreshableListView = require('react-native-refreshable-listview')
var WW=Dimensions.get('window').width;
var WH=Dimensions.get('window').height;
var BUTTONS = [
  '举报',
  '分享',
  '取消',
];
var FILTER_BUTTONS = [
  '未回复',
  '已回复',
  '全部',
];
var CANCEL_INDEX = 2;


var AskList = React.createClass({

  getInitialState: function() {
    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return {
      dataSource: ds.cloneWithRows(['1','2']),
    };
  },

  _pressData: ({}: {[key: number]: boolean}),
  componentDidMount: function() {
    AskStore.addChangeListener(this._onChange);


  },

  componentWillUnmount: function() {
    AskStore.removeChangeListener(this._onChange);
  },
  componentWillMount: function() {

    AskStore.update(this.updateDataSource);
    this.props.navComponent.setNavItems({
      rightItem: {
        component: (
          <TouchableOpacity style={styles.leftNavItem}>
            <Image style={{width: 20, height: 20}} source={require('../images/filter.png')}/>
          </TouchableOpacity>
        ),
        event: function() {
          this._pressFilter();
        }.bind(this)
      }
    });
    this._pressData = {};
  },

  render: function() {
    return (
        <RefreshableListView
          style={styles.container}
          dataSource={this.state.dataSource}
          renderRow={this._renderRow}
          refreshDescription="loading..."
          loadData={this.reloadAsk}
          onEndReached={this.loadMore}
        />

    );
  },

  reloadAsk:function(){
    return '';
  },
  loadMore:function(){
    AskAction.fetchMore();
  },
  updateDataSource: function(data){
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(data)
    })
  },

  _onChange:function(){
    console.log("_onChange");
    AskStore.update(this.updateDataSource);
  },

  _renderRow: function(rowData,sectionID, rowID) {
    return (
        <TouchableHighlight onPress={() => this._pressRow(rowID)}>
        <View>
          <View style={styles.row}>
            <Image style={styles.authorHeader} source={{uri:rowData.head}} />
            <View style={styles.authorInfo}>
              <View style={styles.userInfo}>
                <Text style={styles.userName}>
                  {rowData.name}
                </Text>
                <View style={styles.baseInfo}>
                  <Image style={styles.gender} source={require('../images/gender.png')}/>
                  <Text style={styles.age}>20</Text>
                </View>
              </View>
              <View>
                <Text style={styles.pubTime}>
                  {rowData.time}
                </Text>
              </View>
            </View>
          </View>
          
            <View style={styles.row}>
              <Text style={styles.text}>
               {rowData.content}
              </Text>
            </View>
          
          <View style={styles.row}>

            <Text style={styles.askAttach}>{rowData.hits} 阅读 11 评论</Text>
          </View>

          <View style={[styles.row,styles.footer]}>
            <View style={styles.userInfo}>
            <Image source={require('../images/heart_empty.png')} style={styles.zanImg}/>
            <Text style={styles.zan}>{rowData.favTotalNum}</Text>
            </View>
            <TouchableHighlight onPress={()=>this._pressMore()}>
              <Text style={styles.more}>...</Text>
            </TouchableHighlight>
          </View>

          <View style={styles.separator} />
        </View>
        </TouchableHighlight>
      
    );
  },

  _genRows: function(pressData: {[key: number]: boolean}): Array<string> {
    var dataBlob = [];
    for (var ii = 0; ii < 100; ii++) {
      var pressedText = pressData[ii] ? ' (pressed)' : '';
      dataBlob.push('Row ' + ii + pressedText);
    }
    return dataBlob;
  },

  _pressRow: function(rowID: number) {
    this.props.navigator.push({
      title: '心事1234124',
      component: <AskDetail/>,
      passProps: {
      }
    });
  },
  _pressMore:function(){
    console.log(1234);
     ActionSheetIOS.showActionSheetWithOptions({
      options: BUTTONS,
      cancelButtonIndex: CANCEL_INDEX
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  },
  _pressFilter:function(){
    ActionSheetIOS.showActionSheetWithOptions({
      options: FILTER_BUTTONS,
    },
    (buttonIndex) => {
      this.setState({ clicked: BUTTONS[buttonIndex] });
    });
  }
});


/* eslint no-bitwise: 0 */
var hashCode = function(str) {
  var hash = 15;
  for (var ii = str.length - 1; ii >= 0; ii--) {
    hash = ((hash << 5) - hash) + str.charCodeAt(ii);
  }
  return hash;
};

var styles = StyleSheet.create({
  container:{
    height:WH
  },
  authorHeader:{
    height:30,
    width:30,
    borderRadius:15,
    margin:5
  },
  authorInfo:{
    flex:1,
    flexDirection:'column',
    padding:5
  },
  userInfo:{
    flexDirection:'row'

  },
  userName:{
    color:'#987eb9'

  },
  pubTime:{
    color:'#aaaaaa',
    fontSize:10
  },
  baseInfo:{
    flexDirection:'row',
    backgroundColor:'#b6defa',
    height:16,
    marginTop:2,
    marginLeft:5
  },
  gender:{
    width:15,
    height:15

  },
  age:{
    color:'#ffffff',
    fontSize:12
  },
  askTitle:{
    fontSize:16,
    fontWeight:'bold',
    color:'#505050'
  },
  askAttach:{
    fontSize:10,
    color:'#aaaaaa'
  },
  footer:{
    justifyContent:'space-between'
  },
  zan:{
    paddingLeft:10,
    color:'#aaaaaa',

  },
  zanImg:{
    width:16,
    height:16
  },
  more:{
    color:'#aaaaaa',
    fontSize:16,
    fontWeight:'bold',
    paddingLeft:10,
    paddingRight:10
  },
  row: {
    flexDirection: 'row',
    padding: 10,
    paddingBottom:5,
    backgroundColor: '#F6F6F6',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
  thumb: {
    width: 64,
    height: 64,
  },
  text: {
    flex: 1,
    lineHeight:20,
    color:'#565656',
    fontSize:16
  },
  leftNavItem:{
    margin:10
  }
});

module.exports = AskList;