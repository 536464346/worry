'use strict';
var AppDispatcher = require('../dispatcher/AppDispatcher');
var WorryConstants = require('../constants/WorryConstants');

var ActionTypes = WorryConstants.ActionTypes;

module.exports = {

  fetchMore: function(userName) {
    AppDispatcher.dispatch({
      type: ActionTypes.FETCH_MORE
    });
  }
};
