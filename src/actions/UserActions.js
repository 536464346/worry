'use strict';
var AppDispatcher = require('../dispatcher/AppDispatcher');
var WorryConstants = require('../constants/WorryConstants');

var ActionTypes = WorryConstants.ActionTypes;

module.exports = {

  userNameChange: function(userName) {
    AppDispatcher.dispatch({
      type: ActionTypes.USERNAME_CHANGE,
      value: userName
    });
  },
  passWordChange:function(passwd){

  	AppDispatcher.dispatch({
			type: ActionTypes.PASSWD_CHANGE,
			value: passwd

  	});
  },
  login:function(){
  	AppDispatcher.dispatch({
			type: ActionTypes.L,

  	});
  }

};
