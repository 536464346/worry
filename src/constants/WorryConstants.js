'use strict';
var keyMirror = require('keyMirror');
module.exports = {

  ActionTypes: keyMirror({
  	LOGIN:null,
  	FORGET:null,
  	REG:null,
  	USERNAME_CHANGE:null,
  	PASSWORD_CHANGE:null,
  	FETCH_MORE:null
  })

};