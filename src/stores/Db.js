var RNDBModel = require('react-native-db-models')
 
var DB = {
	"user": new RNDBModel.create_db('user'),
	"askTags":new RNDBModel.create_db('askTags'),
	"ask":new RNDBModel.create_db('ask'),
}
 
module.exports = DB