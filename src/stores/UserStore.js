'use strict';
var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ActionTypes = require('../constants/WorryConstants').ActionTypes;
var assign = require('object-assign');
var DB=require("./Db");
var Api=require("../utils/Api.js");
var md5=require('md5');

var modalVisible=false;
var CHANGE_EVENT = 'change';
var UserStore = assign({}, EventEmitter.prototype, {
  initState:function(){
    return {
          user_name:'guest',
          is_login:false,
          gender:1,
          age:'',
          head:'',
          passwd:'',
          uid:0,
          accessToken:'',
          loginTime:''
        };
  },
  emitChange: function() {
    this.emit(CHANGE_EVENT);
  },

  /**
   * @param {function} callback
   */
  addChangeListener: function(callback) {
    this.on(CHANGE_EVENT, callback);
  },

  /**
   * @param {function} callback
   */
  removeChangeListener: function(callback) {
    this.removeListener(CHANGE_EVENT, callback);
  },

  userInit:function(){
    var that=this;
    DB.user.get_all(function(rs){
      console.log(rs);
      if(rs.totalrows==0){
        var initInfo=that.initState();
        DB.user.add(initInfo,function(){
          return false;
        })
      }
    });
  },
  login:function(cb){
    DB.user.get_all(function(rs){
      for(var i in rs.rows){
          break;
        }
      var user=rs.rows[i];
      Api.request({userName:user.user_name,passwd:md5(user.passwd)},'user/login',function(trs){

        if(trs.code==0){
          
          DB.user.get_all(function(rs){
            for(var i in rs.rows){
              break;
            }

            rs.rows[i].uid=trs.data.uid;
            rs.rows[i].accessToken=trs.data.accessToken;
            rs.rows[i].is_login=true;
            rs.rows[i].loginTime=parseInt(Date.parse(new Date())/1000);
             DB.user.update_id(i,rs.rows[i],function(){
               cb();
             })
          })
        }

      });

    });

  },
  update:function(cb){
    
    DB.user.get_all(function(rs){
        var id=0;

        for(var i in rs.rows){
          break;
        }
        cb(rs.rows[i]) 
      })

  }

});

UserStore.dispatchToken = AppDispatcher.register(function(action) {

  switch(action.type) {

    case ActionTypes.USERNAME_CHANGE:
      DB.user.get_all(function(rs){
        var id=0;
        for(var i in rs.rows){
          break;
        }
        id=i;
        rs.rows[i].user_name=action.value;
         DB.user.update_id(id,rs.rows[i],function(){
           UserStore.emitChange();
         })
      })
      break;
    case ActionTypes.PASSWD_CHANGE:
      DB.user.get_all(function(rs){
        var id=0;
        for(var i in rs.rows){
          break;
        }
        id=i;
        rs.rows[i].passwd=action.value;
         DB.user.update_id(id,rs.rows[i],function(){
           UserStore.emitChange();
         })
      })
      break;

    case ActionTypes.LOGIN:
      UserStore.login(function(){
        UserStore.emitChange();
      });
      
      break;
    default:
      // do nothing
  }

});

module.exports = UserStore;
