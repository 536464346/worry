var md5=require('md5');
var sessionKey='dc59cf294f37d237c1f06240568ffe21';
var baseUrl='http://test2.api.yidianling.com/v1/';
var Api={
	genSign:function(data){
		var keyArr=[];
		for(var i in data){
			keyArr.push(i);
		}
		keyArr.sort(this.comp);
		
		var tempArr=[];
		for(var i in keyArr){
			tempArr.push(keyArr[i]+'='+data[keyArr[i]]);
		}
		return md5(tempArr.join("&")+sessionKey);

	},

	comp:function(a,b){
		if(a>b){
			return -1;
		}
		else if(a==b)
			return 0;
		else 
			return 1;
	},
	request:function(data,api,cb){
		data.ts=parseInt(Date.parse(new Date())/1000);
		var sign=this.genSign(data);

		fetch(baseUrl+api, {
		  method: 'PUT',
		  headers: {
		    'Accept': 'application/json',
		    'Authorization':'Ydl '+sign,
		    'Content-Type': 'multipart/form-data',
		  },
		  body: JSON.stringify(data)
		})
		.then((response)=>response.json())
		.then((rs)=>{cb(rs)});
	},
	isEmpty:function(obj){
		for(var i in obj){
			return false;
		}	
		return true;
	}

};

module.exports=Api;